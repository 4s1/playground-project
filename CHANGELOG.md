# Changelog

## 3.0.0 (2023-01-20)

- This package is now pure ESM. Please [read this](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c).

## 2.1.1 (2022-04-03)

## 2.1.0 (2022-04-02)

### Bug Fixes

- typo

## 2.0.0 (2021-11-21)

### Features

- pure ESM module

### Bug Fixes

- use correct tsconfig.json
