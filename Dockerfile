FROM ghcr.io/4s1-org/node:18.15.0-alpine AS builder

USER ${X_USERNAME}

COPY . .
RUN pnpm i
