# #!/usr/bin/env bash

# set -e
# set -u
# set -o pipefail

# echo "*******************************"
# echo "Create and publish docker image"
# echo "*******************************"

# version=$(jq -r .version package.json)
# latest=0

# while true; do
#     read -p "Tag as $version? " yn
#     case $yn in
#         [Yy]* ) break;;
#         [Nn]* ) exit 0; break;;
#         * ) echo "Please answer y or n.";;
#     esac
# done

# while true; do
#     read -p "Tag as latest? " yn
#     case $yn in
#         [Yy]* ) latest=1; break;;
#         [Nn]* ) latest=0; break;;
#         * ) echo "Please answer y or n.";;
#     esac
# done

# params=()
# params+=(--tag ghcr.io/4s1-org/playground-project:$version)
# if [[ "$latest" -eq 1 ]]; then
#     params+=(--tag ghcr.io/4s1-org/playground-project:latest)
# fi
# docker build "${params[@]}" .

# echo $GH_CR_PAT | docker login ghcr.io -u $GH_CR_USER --password-stdin
# docker push ghcr.io/4s1-org/playground-project:$version

# if [ "$latest" -eq 1 ] ;then
#     docker push ghcr.io/4s1-org/playground-project:latest
# fi
